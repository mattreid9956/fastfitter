// $Id: $
#ifndef FAST_FITTERLIMITS_H
#define FAST_FITTERLIMITS_H 1

// Include files
// STL include
#include <string>
#include <map>
#include <functional> // c++11 std::function

// ROOT
#include "Rtypes.h"
#include "TNamed.h"
#include "TAttLine.h"
#include "TH2.h"
#include "TFormula.h"

// RooFit
#include "RooCmdArg.h"
#include "RooAbsPdf.h"
#include "RooDataHist.h"
#include "RooMsgService.h"
#include "RooGlobalFunc.h"


// Local includes
#include "FitterBase.hpp"
#include "LHCbStyle.hpp"
#include "string_tools.hpp"
#include "FitterPulls.hpp"

/// forward declarations
// ROOT
class TFile;

// RooFit
class RooWorkspace;
class RooAbsData;
class RooDataSet;
// class RooDataHist;
class RooAbsReal;
class RooRealVar;
class RooPlot;
// class RooAbsPdf;
class RooAddPdf;
class RooArgSet;
class RooHist;
class RooFitResult;
//class RooCmdArg;

/** @class Fitter Fitter.hpp Fitter/Fitter.hpp

  Class to perform handle the fitting of unbinned data with one or more
  physical parameters (e.g. reconstructed mass of a particle) to a model PDF,
  constructing sWeights, creating reduced datasets and plotting fit results.
  Note that the model PDF must be of type RooAddPdf (or inherit from it),
  since several methods (include the construction of the sPlot and the 
  plotting methods) require a RooArgList of coefficients and PDFs.

  @author Matthew M Reid
  @date   2014-01-08
 */

namespace Fast // Fiting Analysis Simulataneous Toys
{
    class Fitter : public TNamed//, public FitterLimitsBase
    {
        public:
            // Randomisation enumerator.
            enum RandomMethod{ UNIFORM = 0, GAUSSIAN, CORRELATION, ALL, NONE };
            ClassDef(Fitter,1);
            
            typedef Double_t(*DoubleFun)(Double_t a);
    	    // the following is a dummy variable that seems to be necessary for rootcint to understand the DoubleFun typedef
            //std::function m_currentFunc;
		    DoubleFun m_currentFunc;

            
            /// Standard constructor.
            Fitter( );

            /** Overloaded constructor.
              @param name name of the (TNamed) object.
              @param title title of the (TNamed) object.
             */
            Fitter( const char* name, const char* title );
            

            virtual ~Fitter( ); ///< Destructor



    };
}
#endif // FAST_FITTERLIMITS_H
