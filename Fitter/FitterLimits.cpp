// $Id: $
// Include files
// std libs
#include <iostream>
#include <cmath>
#include <ctime>
#include <sstream>
#include <cassert>
#include <algorithm>    // std::find
#include <iomanip>      // std::setprecision

// ROOT
#include "TCanvas.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TGraph.h"
#include "TLine.h"
#include "TAxis.h"
#include "TPaveText.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TMath.h"
#include "TSystem.h"
#include "TString.h"
#include "TEventList.h"
#include "TSystem.h"
#include "TSystemDirectory.h"
#include "TTreeFormula.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"
#include "TRandom3.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TEntryList.h"

// RooFit
#include "RooWorkspace.h"
#include "RooAbsCollection.h"
#include "RooAbsData.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooAbsPdf.h"
#include "RooAddPdf.h"
#include "RooArgSet.h"
#include "RooAbsBinning.h"
#include "RooConstVar.h"
#include "RooFormulaVar.h"
#include "RooMinuit.h"
#include "RooRandom.h"
#include "RooGaussian.h"
#include "RooUnblindPrecision.h"
#include "RooUnblindUniform.h"
#include "RooCmdArg.h"

// RooStats
#include "RooStats/SPlot.h"

// TMVA
#include "TMVA/Timer.h"

// local
#include "FitterLimits.hpp"
#include "Exceptions.hpp"
#include "ClientTree.hpp"
#include "string_tools.hpp"

//-----------------------------------------------------------------------------
// Implementation file for class : FitterLimits
//
// 2014-01-08 : Matthew M Reid
//-----------------------------------------------------------------------------

using namespace Fast;
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FitterLimits::FitterLimits(  ) 
    : TNamed(), m_nCores(2)
{
}

//-----------------------------------------------------------------------------
// overloaded constructor
//-----------------------------------------------------------------------------
FitterLimits::FitterLimits( const char* name, const char* title)
    : TNamed(name, title), m_nCores(2)
{
}

//=============================================================================
// Add Spectator variables to the dataset
//=============================================================================


//=============================================================================
// Destructor
//=============================================================================
FitterLimits::~FitterLimits()
{
    if ( m_hasOwnership && m_rws )
    {
        // If we created the RooWorkspace, then we are in charge of deleting it.
        // If it was loaded from a TFile, then it will be deleted automatically
        // when the file is closed.
        delete m_rws; m_rws=0;
    }
    //m_file->Close();
    delete m_file; m_file=0;
}
//=============================================================================
