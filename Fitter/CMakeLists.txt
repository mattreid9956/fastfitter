# Setup the ROOT compilation for adding to the main fitter_b2jpsikshh library

# Load some basic macros which are needed later on
configure_file(FitterConfig.h.in "${CMAKE_CURRENT_BINARY_DIR}/FitterConfig.h" @ONLY)

#include(FindROOT)
set(INCLUDE_DIRECTORIES
	${ROOT_INCLUDE_DIR} 
	${CMAKE_CURRENT_SOURCE_DIR} 
	${INCLUDE_DIRECTORIES}
	../
)

MESSAGE(STATUS "Include libraries are ${INCLUDE_DIRECTORIES}")

# List of source files
set ( Fitter_SRCS  ToyStudy.cpp string_tools.cpp LHCbStyle.cpp ModelBase.cpp  RooCruijff.cpp  RooCruijffSimple.cpp  RooApollonios.cpp  RooAmorosoPdf.cpp  RooHypatia.cpp  RooHypatia2.cpp  ClientTree.cpp  Exceptions.cpp  Fitter.cpp  SimultaneousFitter.cpp FitterPulls.cpp FitterLikesRatioPlot.cpp
)

string ( REPLACE ".cpp" ".hpp" Fitter_HEADERS "${Fitter_SRCS}" )
# Add back any pure virtual headers that have no .cc implementation
set ( Fitter_HEADERS FitterBase.hpp ${Fitter_HEADERS} )


# Not the recommended way but this is always an option,
# probably not best though when dealing with ROOT
#FILE(GLOB_RECURSE UTILS_SOURCE_FILE_LIST "*.cpp")
#FILE(GLOB_RECURSE UTILS_HEADER_FILE_LIST "*.hpp")

# set everything needed for the root dictonary and create the dictionary
set(Fitter_LINKDEF FitterLinkDef.h )
set(Fitter_DICTIONARY ${CMAKE_CURRENT_BINARY_DIR}/Dict.cxx) 
ROOT_GENERATE_DICTIONARY("${Fitter_HEADERS}" "${Fitter_LINKDEF}" "${Fitter_DICTIONARY}" "${INCLUDE_DIRECTORIES}")

# add the dictionary to the list of source files
set (Fitter_SRCS ${Fitter_SRCS} ${Fitter_DICTIONARY}) 

# Set the library version in the main CMakeLists.txt
set (Fitter_LIBRARY_PROPERTIES ${Fitter_LIBRARY_PROPERTIES}
    VERSION   "${FITTER_VERSION_MAJOR}"
    SOVERSION "${FITTER_VERSION_MINOR}"
    SUFFIX ".so"
)

set(LINK_DIRECTORIES
${ROOT_LIBRARY_DIR}
)

# List the required root directories that shall persist 
set( ADD_LIBRARIES ${ROOT_LIBRARIES} EG Gui Hist TMVA TreePlayer GenVector RooFitCore RooFit RooStats Cintex MathMore gsl gslcblas m )

link_directories( ${LINK_DIRECTORIES})



############### build the library #####################
add_library(Fitter SHARED ${Fitter_SRCS} ${Fitter_HEADERS} FitterConfig.h.in)

target_link_libraries(Fitter ${ADD_LIBRARIES} )

set_target_properties(Fitter PROPERTIES
  PUBLIC_HEADER "${Fitter_HEADERS};${CMAKE_CURRENT_BINARY_DIR}/FitterConfig.h" 
  ${Fitter_LIBRARY_PROPERTIES} 
  )
#set_target_properties(Fitter PROPERTIES ${Fitter_LIBRARY_PROPERTIES})
 
install(TARGETS Fitter
  # IMPORTANT: Add the Fitter library to the "export-set"
  EXPORT FitterTargets
  RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}" COMPONENT bin
  LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}" COMPONENT shlib
  PUBLIC_HEADER DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/Fitter" # used to install the headers
    COMPONENT dev
    )


############### install the library ###################
#install(TARGETS Fitter DESTINATION ${CMAKE_BINARY_DIR}/lib)
