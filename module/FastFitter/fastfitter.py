import os
# Locate this module on the filesystem
__packagename_path = os.path.abspath( os.path.dirname( __file__ ) )
__packagename_so = os.path.join( __packagename_path, "/usr/local/fastfitter/lib/libFitter.so" )

from ctypes import *
cdll.LoadLibrary( "/usr/local/fitter/lib/x86_64-linux-gnu/libFitter.so" )

# Always need to import the shared object and extract useful namespace
import PyCintex
PyCintex.Cintex.Enable()
PyCintex.loadDictionary( "/usr/local/fastfitter/lib/libFitter.so" )

Fitter = PyCintex.gbl.Fast.Fitter # useful namespace
SimultaneousFitter = PyCintex.gbl.Fast.SimultaneousFitter # useful namespace
ModelBase = PyCintex.gbl.Fast.ModelBase # useful namespace
FitterPulls = PyCintex.gbl.Fast.FitterPulls # useful namespace
ClientTree = PyCintex.gbl.Fast.ClientTree # useful namespace
FitterLikesRatioPlot = PyCintex.gbl.Fast.FitterLikesRatioPlot # useful namespace
LHCbStyle = PyCintex.gbl.Fast.LHCbStyle
ToyStudy = PyCintex.gbl.Fast.ToyStudy
string_tools = PyCintex.gbl.string_tools

