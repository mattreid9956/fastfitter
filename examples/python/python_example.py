#!/usr/bin/python

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# UNTIL LOADED TO THE LIBRARY PATH THIS MUST BE RAN FROM WITHIN THE build/Fitter directory
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# Always need to import the shared object and extract useful namespace
import PyCintex, ROOT
from ROOT import RooFit, RooRealVar, RooGaussian, RooDataSet, RooArgList, RooTreeData

#PyCintex.Cintex.Enable()
#PyCintex.loadDictionary( 'libFitter.so' )

#Fitter = PyCintex.gbl.Fast.Fitter # useful namespace
#SimultaneousFitter = PyCintex.gbl.Fast.SimultaneousFitter # useful namespace
###############################################################################

# Here we can use the functions
Bd_blind = False
Bs_blind = False

fitter = SimultaneousFitter("B2JpsiKShhFit", "B0 --> Jpsi KS h' h Fit")
fitter.createWS( "Desktop", "Desktop" );


fitter.setGlobalKillBelowError( RooFit.FATAL )
fitter.setnCPU( 4 ); # sets the number of CPUs to run the minimisation in parallel
massvar = "B_Mass_Constr" # Setup the name of fit axix.
xmin = 5180.0
xmax = 5500.0 # set the range of fit axis

fitter.set1DFitVarName( massvar )
fitter.make1DFitVar( xmin, xmax, "MeV/c^{2}", "B^{0}_{(s)} --> J/#psi K^{0}_{S} #pi^{+} #pi^{-}" )
fitter.setPlotBins( massvar, 80 )
#================================================================================


#================================================================================
# Make all the pdfs I need
#================================================================================
# Bd signal LL model
fitter.makeSingleGauss("Bd_signalLL", 5280, 5250, 5300, #mean
        8, 0, 20 #sigma_core
        )
# Bd signal DD model
fitter.makeSingleGauss("Bd_signalDD", 5280, 5250, 5300, #mean
        10, 0, 20 #sigma_core
        )

# Bs signal LL model
fitter.makeSingleGauss("Bs_signalLL", 5366, 5350, 5375, #mean
        10, 0, 20 #sigma_core
        )
# Bs signal DD model
fitter.makeSingleGauss("Bs_signalDD", 5366, 5350, 5375, #mean
        13, 0, 20 #sigma_core
        )

fitter.makeChebychev( "combinatoric_jpsiKSpipiLL", -0.0045, -1.0, 1.0 )
fitter.makeChebychev( "combinatoric_jpsiKSpipiDD", -0.0045, -1.0, 1.0 )

#================================================================================
# Import the data that you need
#================================================================================
inputFileNameLL = "/home/matt/C++/fitter/examples/data/ExampleDataLL.root"
fileLL = ROOT.TFile.Open( inputFileNameLL, "READ" )
if fileLL is None or fileLL.IsZombie()  :
    print "ERROR: Failed to open candidate file " + inputFileNameLL + " for reading\n"



llpipiTupleName = "treeLL";
llpipiTree = fileLL.Get( llpipiTupleName )
if llpipiTree is None :
    print "ERROR: Failed to retrieve candidate nTuple " + llpipiTupleName + " from file" + inputFileNameLL


fitter.makeMassDataSet( llpipiTree, massvar, "JpsiKSpipiLL", "JpsiKSpipiLL" ) #, place cuts here("D0K_PIDK>0 && D0Pi_PIDK<4 && KstarK_PIDK>"+v1PIDCut+" && KstarPi_PIDK<"+v2PIDCut).c_str());
fileLL.Close();

inputFileNameDD = "/home/matt/C++/fitter/examples/data/ExampleDataDD.root"
fileDD = ROOT.TFile.Open( inputFileNameDD, "READ" )
if fileDD is None or fileDD.IsZombie():
    print "ERROR: Failed to open candidate file " + inputFileNameDD + " for reading.\n"

ddpipiTupleName = "treeDD"
ddpipiTree = fileDD.Get( ddpipiTupleName )
if ddpipiTree is None :
    print "ERROR: Failed to retrieve candidate nTuple " + ddpipiTupleName + " from file" + inputFileNameDD

# Follows MakeMassDataSet( tree, massName, name, title, cuts );
fitter.makeMassDataSet( ddpipiTree, massvar, "JpsiKSpipiDD", "JpsiKSpipiDD" )
fileDD.Close()  # close the file no longer needing it!

fitter.combineDataSets()
#================================================================================

#================================================================================
# Create PDFs and then add by csv the pdfs that should be used for the mass
# distribution imported above.
#================================================================================
fitter.createPDFSets()
fitter.addPDFs( "JpsiKSpipiLL", "Bd_signalLL,Bs_signalLL,combinatoric_jpsiKSpipiLL" )
fitter.addPDFs( "JpsiKSpipiDD", "Bd_signalDD,Bs_signalDD,combinatoric_jpsiKSpipiDD" )

fitter.addYields( "JpsiKSpipiLL" )
fitter.addYields( "JpsiKSpipiDD" )

fitter.buildAddPdfs()
fitter.buildModel()
#================================================================================


#================================================================================
# Set parameter constraints and and anything that is constant or has Gaussian
# constraints. Can also set blinding of variables here too.
#================================================================================
# signal means are separated by their pdg difference
fitter.addConstraint("Bs_signalLL_mu","@0 + 87.35","Bd_signalLL_mu")
fitter.addConstraint("Bs_signalDD_mu","@0 + 87.35","Bd_signalDD_mu")
# constrain the common means between DD and LL modes
fitter.addConstraint("Bd_signalDD_mu","@0","Bd_signalLL_mu")

# add a new parameter to relate the LL and DD modes
fitter.addParameter( "LLoverDD", 0.0, 4.0 )
fitter.addConstraint("Bd_signalLL_sigma0","@0*@1","Bd_signalDD_sigma0,LLoverDD")
fitter.addConstraint("Bs_signalLL_sigma0","@0*@1","Bs_signalDD_sigma0,LLoverDD")

# width ratio of double gaussian signal is constant at 2.1 and equal for
# Bs and Bd
fitter.addGaussianConstraint("Bs_signalDD_sigma0", 13, 3)

# core fraction of double gaussian signal is constant at 0.87 and equal for
# Bs and Bd
#fitter.setParameterConstant("Bd_signal_coreFrac", 0.87 )

#blind the Bs --> JpsiKSpipi(LL/DD) yield
if Bd_blind :
    fitter.blindParameter("Bd_signalLL_JpsiKSpipiLL_Yield", 30, 30)
    fitter.blindParameter("Bd_signalDD_JpsiKSpipiDD_Yield", 30, 30)

if Bs_blind :
    fitter.blindParameter("Bs_signalLL_JpsiKSpipiLL_Yield", 30, 30)
    fitter.blindParameter("Bs_signalDD_JpsiKSpipiDD_Yield", 30, 30)

#================================================================================


#================================================================================
# Finally perform the fit to the data and the plot some results.
#================================================================================
fitter.performFit();
ntot = fitter.sumYields();
print "Yields: %.1f" % ntot
#================================================================================


